<?php

class ProductModel extends Model
{
    public static function getCartDetails() 
    {
        $cart = [];
        if (isset($_SESSION['cart'])) {
            $products_ids = array_keys($_SESSION['cart']);
            $products_str = join(", ", $products_ids);
            $sql = "SELECT product_id, name, price FROM product WHERE product_id in ($products_str)";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
            $products = $stmt->fetchAll();
            $totalQuantity = 0;
            $totalPrice = 0;
            foreach($products as $key=>&$product) {
                $product['quantity'] = $_SESSION['cart'][$product['product_id']]['quantity'];
                $product['totalprice'] = $product['quantity'] * $product['price'];
                $totalQuantity += $product['quantity'];
                $totalPrice += $product['totalprice'];
            }
            $cart['products'] = $products;
            $cart['totalQuantity'] = $totalQuantity;
            $cart['totalPrice'] = $totalPrice;
        }
        return $cart;
    }
    
    public static function getCartTotals() 
    {
        $cartTotals = [];
        $quantity = 0;
        $price = 0;
        if (isset($_SESSION['cart'])){
            foreach($_SESSION['cart'] as $productItem) {
                $quantity += $productItem['quantity'];
                $price += $productItem['quantity'] * $productItem['price'];
            }
        }
        $cartTotals['quantity'] = $quantity;
        $cartTotals['price'] = $price;
        return $cartTotals;
    }
    
    public static function getPrice($product_id)
    {
        $price = 0;
        $sql = "SELECT price FROM product WHERE product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$product_id]);
        $price = $stmt->fetch();
        return $price['price'];
    }
    
}
