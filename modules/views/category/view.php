<div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3">
        <div class="list-group">
            <span class="list-group-item list-group-item-success">Категорії</span>
            <?php foreach($categories as $key => $categoryItem): ?>
                <a class="list-group-item <?php if ($categoryItem['category_id'] == $category->category_id): ?>active<?php endif; ?>" href="/category/<?= $categoryItem['category_id']; ?>">
                    <?php echo $categoryItem["name"]; ?>
                </a>
            <?php endforeach;?>
        </div>
    </div>
    <div class="col-xs-12 col-md-8 col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><?= $category->name; ?></h2>
            </div>
            <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                        <p>
                            <a href="<?= $category->imageFiles['600_image']; ?>"><img src="<?= $category->imageFiles['200_image']; ?>" alt=""  class="media-object"></a>
                        </p>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Опис</h4>
                      <p><?= $category->description; ?></p>
                    </div>
                </div>
                
                <table class="table table-bordered">
                    <tr class="success">
                        <th colspan="4">Товари даної категорії</th>
                    </tr>
                    <tr class="success">
                        <th>Зображення</th>
                        <th>Назва товару</th>
                        <th>Ціна товару</th>
                        <th class="action-column"></th>
                    </tr>
                    <?php foreach ($category->products as $product): ?>
                    <tr>
                        <td class="text-center">
                            <span class="thumb"><img src="<?= $product['image_files']['100_image']; ?>"></span>
                        </td>
                        <td>
                            <a href="/product/<?= $product['product_id']; ?>">
                                <?= $product['name']; ?>
                            </a>
                        </td>
                        <td>
                            <?= $product['price']; ?>
                        </td>
                        <td>
                            <button id="add_<?= $product['product_id'] ?>" onclick="addToCart(this);" class="btn btn-default btn-primary" data-toggle="modal" data-target="#myModal">
                                Додати
                            </button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Товар додано до кошика</h4>
      </div>
      <div class="modal-body">
          <p>Всього в кошику <span class='cart_quantity'></span> товарів</p>
          <p>Загальною вартістю <span class='cart_price'></span> грн</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Продовжити покупки</button>
        <a href="/cart" class="btn btn-primary">Перейти до кошика</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
   function addToCart(obj) {
       var id = obj.id;
       var reg = /^add_([\d]+)$/;
       var res = reg.exec(id);
       $.ajax({
           type: "POST",
           url: "/product/addToCart",
           data: {
               "product_id": res[1],
           },
           success: function(data) {
               var obj = JSON.parse(data);
               $(".cart_quantity").text(obj.quantity);
               $(".cart_price").text(obj.price);
           }
        
        });
       
    }
</script>