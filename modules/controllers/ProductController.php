<?php

class ProductController extends FrontController 
{

    public function actionView($parameters = []) 
    {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $params["product"] = $product;
        $content = (new View('product/view', $params))->getHTML();
        $this->view->setParam("title", $product->name);
        $this->view->setParam("content", $content);
    } 
    
    public function actionList($parameters = []) 
    {
        $products = ProductModel::getProduct($parameters[0]);
        $params["products"] = $products;
        $content = (new View('product/list', $params))->getHTML();
        $this->view->setParam("title", "Тут буде заголовок списку статей");
        $this->view->setParam("content", $content);       
    }
    
    public function actionAddToCart($parameters = [])
    {
        $product_id = $_POST['product_id'];
        if (isset($_SESSION['cart'][$product_id]['quantity'])) {
            $_SESSION['cart'][$product_id]['quantity']++;
        } else {
            $_SESSION['cart'][$product_id]['quantity'] = 1;
            $_SESSION['cart'][$product_id]['price'] = ProductModel::getPrice($product_id);
        }
        
        $cartTotals = ProductModel::getCartTotals();
        echo json_encode($cartTotals);
        die;
    }
    
    public function actionUpdateCart($parameters = []) 
    {
        $product_id = $_POST['product_id'];
        $quantity = $_POST['quantity'];
        $price = $_SESSION["cart"][$product_id]["price"];
        $_SESSION["cart"][$product_id]["quantity"] = $quantity;
        $cartTotals = ProductModel::getCartTotals();
        $cartTotals['itemTotalPrice'] = $quantity * $price;
        $cartTotals['itemQuantity'] = $quantity;
        echo json_encode($cartTotals);
        die;
    }
    
    public function actionViewCart($parameters = []) 
    {
        $cart = ProductModel::getCartDetails();
        $params["cart"] = $cart;
        $content = (new View('product/cart', $params))->getHTML();
        $this->view->setParam("title", "Кошик");
        $this->view->setParam("content", $content);       
    }
    
    public function actionGetCart($parameters = [])
    {
        $cart = ProductModel::getCartDetails();
        echo json_encode($cart);
        die;
    }
    
    public function actionSendOrder($parameters = [])
    {
        $to    = ADMIN_EMAIL;
        $subject = 'Замовлення Квіткового салону';
        $message = $_POST['order'];
        if (mail($to, $subject, $message)) {
            echo true;
        } else {
            echo false;
        }
        die;
    }
    /*
    public function actionDeleteSession()
    {
        if (isset($_SESSION["cart"])) {
            $_SESSION["cart"] = array();
        }
        $cartTotals = ProductModel::getCartTotals();
        echo json_encode($cartTotals);
        die;
    }
    */
}
