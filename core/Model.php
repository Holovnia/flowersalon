<?php

class Model 
{
    public function initObjectFromArray($array) 
    {
        foreach ($array as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }    
}
