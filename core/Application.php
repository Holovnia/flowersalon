<?php
class Application 
{   
    protected $path;
    protected $controllerClass;
    protected $actionMethod;
    
    protected $controller;
    protected $parameters;
      
    public function run()
    {
        $this->init();
        $this->route();
        $this->action();
        $this->render();
    }

    protected function init()
    {
        DataBase::Connect();
        session_start();        
    }

    protected function route()
    {
        $routes = include(ROOT . "/config/routes.php");
        $uri = trim($_SERVER["REQUEST_URI"], "/");
        $path = "home/page404";
        foreach($routes as $key => $route) {
            if(preg_match("*^$key$*", $uri)) {
                $path = preg_replace("*$key*", $route, $uri);
                break;
            }
        }
        $this->path = $path;
        $parts = [];
        $parts = explode("/", $this->path);
        $this->controllerClass = ucfirst(array_shift($parts)) . "Controller";
        $this->actionMethod = 'action' . ucfirst(array_shift($parts));
        $this->parameters = $parts;        
    }
    
    protected function action() 
    {
        $className = $this->controllerClass;
        $action = $this->actionMethod;
        $this->controller = new $className;
        $this->controller->$action($this->parameters);
    }
    
    protected function render() 
    {
        $this->controller->render();
    }
    
}
