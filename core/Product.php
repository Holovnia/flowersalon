<?php

class Product extends Model
{
    public $product_id;
    public $name;
    public $description;
    public $category_id;
    public $category_name;
    public $sku;
    public $image;
    public $imageFiles;
    public $price;
    public $sort_order;
    public $status;
    public $url;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;
    
    public function __construct($product_id = 0) 
    {
        if ($product_id > 0) {
            $productArray = $this->getProductById($product_id);
            $this->initObjectFromArray($productArray);
            $this->imageFiles = $this->setImageFiles();
        }
    }
    
    public function setImageFiles()
    {
        $sizes = [600, 400, 200, 100];
        $defaultImage = '/images/default.gif';
        $imageFiles = [];
        foreach ($sizes as $size) {
            $fileName = '/images/product/' . $this->product_id . '/' . $size . '_' . $this->image;
            $imageFiles[$size . '_image'] = ($this->image) ? $fileName : $defaultImage;
        }
        return $imageFiles;
    }
    
    public function getProductById($product_id) 
    {
        $result = [];
        $sql = "SELECT a.*, b.name AS category_name FROM product AS a "
                . "LEFT JOIN category AS b "
                . "ON a.category_id=b.category_id "
                . " WHERE a.product_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$product_id]);
        return $stmt->fetch();
    }
    
    public static function getProducts($category_id = 0) 
    {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = DataBase::handler()->query("SELECT * FROM product $where;");
        $products = $st->fetchAll();
        $sizes = [600, 400, 200, 100];
        $defaultImage = '/images/default.gif';
        foreach ($products as &$product) {
            foreach ($sizes as $size) {
                $fileName = '/images/product/' . $product['product_id'] . '/' . $size . '_' . $product['image'];
                $product['image_files'][$size . '_image'] = ($product['image']) ? $fileName : $defaultImage;
            }
        }
        return $products;
    }    
    
}
