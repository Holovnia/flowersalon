<?php
class FrontController extends Controller
{   

    public function __construct() 
    {
        $params = [];
        $params['categories'] = Category::getCategories();
        $params['cartTotals'] = ProductModel::getCartTotals();
        $this->view = new View('index', $params);
    }
    
}
