<?php

    spl_autoload_register(function($className){
        $dirs = [
            ROOT . "/core/",
            ROOT . "/modules/controllers/",
            ROOT . "/modules/models/",
        ];
        foreach($dirs as $dir) {
            if (is_file($dir . $className . ".php")) {
                include $dir . $className . ".php";
                break;
            }
        }
    });