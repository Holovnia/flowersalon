-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 14 2017 г., 00:26
-- Версия сервера: 5.7.16-log
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `flowersalon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `url` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`category_id`, `name`, `description`, `image`, `sort_order`, `status`, `url`, `meta_description`, `meta_keyword`) VALUES
(1, 'Троянда ', 'Найвеличніша квітка всіх часів і народів – Роза. Спробуйте повернутися подумки в магазин де продаються одні троянди, що ви відчуваєте ?! Запах? Насолода? Вас покриває почуття романтики. Відразу згадується казка, де дві людини люблять один одного сидять на алеї троянд і задовольняються своїми почуттями любові, це прекрасні моменти, де без квітів, а саме троянд, просто ніяк. І лише ці почуття залишаються у нас у свідомості, коли ми згадуємо такі моменти, вони бувають рідко, і залишаються у нас в душі на все життя.', '', 1, 1, 'category-1', '', ''),
(2, 'Піон', 'У квіткарів півонії користуються заслуженою популярністю. За красою квіток та декоративної листі їм по праву належить одне з перших місць серед садових багаторічників. Великі, пастельних або яскравих забарвлень квітки гарні й на кущі, і в зрізку, дивно приємний їх аромат. Ажурна пишна листя зберігається до пізньої осені, коли з темно-зеленою стає багряної.', '', 2, 1, 'category-2', '', ''),
(3, 'Category 3', 'Hymenaeos facilisis pede felis pharetra nascetur; Metus litora nec nostra... Montes non laoreet facilisis ante fermentum lobortis... Imperdiet diam a imperdiet! Suspendisse posuere facilisi nullam cum aliquam: Fusce quis hac curabitur, urna felis ultrices tempor... Morbi facilisis eget porta porta fermentum porta. Quis lacinia aptent commodo duis natoque interdum ligula; Hymenaeos fermentum sagittis tempus vestibulum? Pharetra auctor nostra class: Nostra nisl tristique magnis natoque vel; Vivamus phasellus condimentum, suspendisse malesuada urna proin ridiculus primis... ', '', 3, 1, 'category-3', '', ''),
(4, 'Category 4', ' Sapien vitae faucibus dis nascetur semper lorem at! Duis ridiculus cubilia torquent lacus integer: Praesent blandit fringilla tempus pulvinar torquent nascetur dolor, posuere pharetra platea tristique elit; Suscipit convallis lorem sit libero purus nec? Semper curae; diam varius conubia; Facilisis lectus egestas sodales class! Aptent maecenas venenatis bibendum!', '', 4, 1, 'category-4', '', ''),
(5, 'Category 5', 'Imperdiet diam a imperdiet! Suspendisse posuere facilisi nullam cum aliquam: Fusce quis hac curabitur, urna felis ultrices tempor... Morbi facilisis eget porta porta fermentum porta. Quis lacinia aptent commodo duis natoque interdum ligula; Hymenaeos fermentum sagittis tempus vestibulum? Pharetra auctor nostra class: Nostra nisl tristique magnis natoque vel; Vivamus phasellus condimentum, suspendisse malesuada urna proin ridiculus primis... ', '', 5, 1, 'category-5', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_icon` varchar(255) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `url` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`product_id`, `name`, `description`, `category_id`, `sku`, `image`, `image_icon`, `price`, `sort_order`, `status`, `url`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'Троянда біла', 'Белы розы – удивительные цветы, которые имеют почти сакральное значение. Он символизируют чистоту, невинность, искренность, настолько  глубокие чувства, что их просто невозможно выразить словами. А ещё – это символ вечной любви, поэтому белые розы – обязательный элемент свадебного букета.', 1, '001', 'c798100c54db272bd9cd46005fd06977.jpg', '/images/icon/flowers_rosa_white.jpg', '35.00', 0, 1, 'product-1', '', '', ''),
(2, 'Троянда Bombastic', 'Роза Bombastic – один из самых трепетных и нежных цветов в коллекции матушки-природы. Этот пышный цветок обладает тугими бутонами мягкого бежево-розового тона и тонким ароматом.', 1, '002', '8f4ab9f64d15f871d48f88aa34022269.jpg', '/images/icon/flowers_rose_spray.jpg', '45.00', 0, 1, 'product-2', '', '', ''),
(3, 'Троянда жовта', 'Символіка жовтих квітів, як правило, досить неоднозначна. З одного боку їх вважають квітами розлуки чи зради, а з іншого — символом успіху і багатства. Ця різниця у трактуванні викликана тим, що в різних країнах значення кольорів часто не збігаються. В Японії, наприклад, жовтий колір асоціюється із золотом і Сонцем і означає щастя, багатство, благополуччя і поваги. ', 1, '003', 'b485ff6427aa3ea313ddc5fe113e712c.jpg', '/images/icon/flowers_rosa_yellow.jpg', '50.00', 0, 1, 'product-3', '', '', ''),
(4, 'Троянда червона', 'Червоні троянди символізують любов, красу і романтику. Про них складено чимало поем, вихваляють високі почуття. Ці чудові квіти зачаровують нас своїми ніжними пелюстками відтінку пристрасті. Червоні троянди гіпнотично приковують до себе увагу, дарують відчуття радості і свята. Червона троянда - це більше, ніж просто квітка. Це ідеальний символ любові і пристрасних почуттів. Вона уособлює любов незалежно від віросповідання чи національності. ', 1, '004', '2a21738d9a476ffdcd3845871b13ee23.jpg', '/images/icon/flowers_rosa_red.jpg', '30.00', 0, 1, 'product-4', '', '', ''),
(5, 'Піон червоний', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 2, '', '63436c964fad79018a6dbf90eae0eacf.jpg', '', '100.00', 0, 1, 'product-5', '', '', ''),
(6, 'Піон рожевий', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 2, '', '311675060f012172504cd5075c62a6d8.jpg', '', '60.00', 0, 1, 'product-6', '', '', ''),
(7, 'Піон білий', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 2, '', 'd2e5a65233e7844d3ea33321fc8ff70f.jpg', '', '70.00', 0, 1, 'product-7', '', '', ''),
(8, 'Product 8', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 3, '', '', '', '70.00', 0, 0, 'product-8', '', '', ''),
(9, 'Product 9', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 3, '', '', '', '1900.00', 0, 0, 'product-9', '', '', ''),
(10, 'Product 10', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 3, '', '', '', '4440.00', 0, 1, 'product-10', '', '', ''),
(11, 'Product 11', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 3, '', '', '', '6600.00', 0, 0, 'product-11', '', '', ''),
(12, 'Product 12', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 4, '', '', '', '6000.00', 0, 1, 'product-12', '', '', ''),
(13, 'Product 13', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 4, '', '', '', '4890.00', 0, 1, 'product-13', '', '', ''),
(14, 'Product 14', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 4, '', '', '', '3034.00', 0, 1, 'product-14', '', '', ''),
(15, 'Product 15', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', 5, '', '', '', '1300.00', 0, 1, 'product-15', '', '', ''),
(16, 'Піон бордовий', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 2, '', '3ca1f4f13f3345fea800c3da0f95ea68.jpg', '', '105.00', 0, 1, 'product-16', '', '', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
