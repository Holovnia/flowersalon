<?php

    define("ROOT", __DIR__);
    define("D_ROOT", $_SERVER["DOCUMENT_ROOT"]);
    include("../config/config.php");
    include(ROOT . "/core/autoload.php");

    (new AdminApplication())->run();