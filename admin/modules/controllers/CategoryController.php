<?php

class CategoryController extends AdminController 
{
    
    public function actionSubmit($parameters = []) 
    {
        $category = new CategoryModel;
        $action = $_POST['action'];
        $array = $_POST;
        $category->initObjectFromArray($array);
        if ($action == 'edit') {
            $category->update();
        } elseif ($action == 'create') {
            $category->create();
        } elseif (in_array($action, ["activate", "deactivate"])) {
            $status = (int)($action == 'activate');
            CategoryModel::changeStatus($status);
        }
        header("Location: /admin/categories/");
        die;
    }
    
    public function actionView($parameters = []) 
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $params["category"] = $category;   
        $content = (new View('category/view', $params))->getHTML();
        $this->view->setParam("title", $category->title);
        $this->view->setParam("content", $content);
    }
    
    public function actionEdit($parameters = []) 
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $params["category"] = $category;
        $params["action"] = "edit";
        $content = (new View('category/form', $params))->getHTML();        
        $this->view->setParam("title", "Редагування категорії");
        $this->view->setParam("content", $content);
    }

    public function actionAdd($parameters = []) 
    {
        $params["action"] = "create";
        $content = (new View('category/form', $params))->getHTML();
        $this->view->setParam("title", "Додавання категорії");
        $this->view->setParam("content", $content);
    }
    
    public function actionList($parameters = []) 
    {
        $params['categories'] = CategoryModel::getCategories();
        $content = (new View('category/list', $params))->getHTML();
        $this->view->setParam("title", "Список категорій");
        $this->view->setParam("content", $content);       
    }
   
}
