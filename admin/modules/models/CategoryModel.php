<?php
class CategoryModel extends Category
{

    public function update() 
    {
        $oldImage = $this->getImage();
        if ($newImage = Image::setImage('category/' . $this->category_id, $oldImage)) {
            $this->image = $newImage;
        } else {
            $this->image = $oldImage;
        };
        $sql = "UPDATE category SET name=?, "
                . "description=?, "
                . "image=?,"
                . "status=?,"
                . "url=?, "
                . "meta_description=?, "
                . "meta_keyword=? "
                . "WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->image,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->category_id,
        ]);
        return $this->category_id;
    }
    
    public function create() 
    {
        $sql = "INSERT INTO category("
                . "name, "
                . "description, "
                . "status, "
                . "url, "
                . "meta_description, "
                . "meta_keyword "
                . "VALUES (?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword
        ]);
        return DataBase::handler()->lastInsertId();
        $this->image = Image::setImage('category/' . $this->category_id);
        $this->updateImage();
        return $this->category_id;
    }
       
    public static function changeStatus($status) 
    {
        if (isset($_POST['category']) && !empty($_POST['category'])) {
            $categories = $_POST['category'];
            $str = join(", ", $categories);
            $sql = "UPDATE category SET status=" . (int)$status . " WHERE category_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }

    private function updateImage()
    {
        $sql = "UPDATE category SET image=? WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->image,
            $this->category_id,
        ]);
        return $this->category_id;        
    }
    
    private function getImage()
    {
        $sql = "SELECT image FROM category WHERE category_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$this->category_id]);
        $result = $stmt->fetch();
        return $result['image'];
    }
}
