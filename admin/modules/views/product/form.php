<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <h2>Форма "Товар"</h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php if ($action == 'edit'): ?>
                    Редагування товару
                <?php else: ?>
                    Створення нового товару
                <?php endif; ?>
            </div>
            <div class="panel-body">
                <form method='post' action='/admin/product/submit' enctype="multipart/form-data">   
                    <div class="form-group">
                        <label for="form-product-id">Product ID</label>
                        <input type='text' class="form-control"  readonly="readonly" name='product_id' id="form-product-id" value='<?php echo $product->product_id; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-product-name">Product Name</label>
                        <input type='text' class="form-control"  name='name' id="form-product-name" value='<?php echo $product->name; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-category">Category</label>
                        <select name='category_id' id="form-category" class="form-control" >
                            <?php foreach($categories as $category): ?>
                               <?php if ($product->category_id == $category['category_id']): ?>
                               <option value="<?= $category['category_id']?>" selected='selected'><?= $category['name']?></option>
                               <?php else: ?>
                               <option value="<?= $category['category_id']?>"><?= $category['name']?></option>
                               <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="form-description">Product Description</label>
                        <textarea name='description' id="form-description" class="form-control" ><?php echo $product->description; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="form-sku">Product SKU</label>
                        <input type='text' class="form-control"  name='sku' id="form-sku" value='<?php echo $product->sku; ?>' placeholder="sku">
                    </div>
                    <div>
                        <img src="<?= $product->imageFiles['200_image'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="form-product-image">Product Image</label>
                        <input type='file' class="form-control"  name='image' id="form-product-image">
                    </div>
                    
                    <div class="form-group">
                        <label for="form-product-price">Product Price</label>
                        <input type='text' class="form-control"  name='price' id="form-product-price" value='<?php echo $product->price; ?>' placeholder="price">
                    </div>

                    <div class="form-group">
                        <label for="form-status">Status</label>
                        <?php if ($product->status): ?>
                        <input type='checkbox' class="form-control"  name='status' id="form-status" value="1" checked="checked">
                        <?php else: ?>
                        <input type='checkbox' class="form-control" name='status' value="1">
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label for="form-product-url">Product url</label>
                        <input type='text' class="form-control" name='url' id="form-product-url" value='<?php echo $product->url; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-meta-title">META title</label>
                        <input type='text' class="form-control" name='meta_title' id="form-meta-title" value='<?php echo $product->meta_description; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-meta-description">META description</label>
                        <input type='text' class="form-control" name='meta_description' id="form-meta-description" value='<?php echo $product->meta_description; ?>'>
                    </div>

                    <div class="form-group">
                        <label for="form-meta-keyword">META description</label>
                        <input type='text' class="form-control" name='meta_keyword' id="form-meta-keyword" value='<?php echo $product->meta_keyword; ?>'>
                    </div>

                    <div class="form-group">
                        <input type='hidden' name='action' value='<?php echo $action; ?>'>
                        <input type='submit' class="btn btn-default" value="Створити">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>                